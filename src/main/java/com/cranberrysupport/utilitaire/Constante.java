package com.cranberrysupport.utilitaire;

public class Constante {

	public static final String MESSAGE_AFFICHAGE_UTILISATEUR = "Nom d'utilisateur:";
	public static final String MESSAGE_AFFICHAGE_MDP = "Mot de passe:";
	public static final String MESSAGE_AFFICHAGE_EXEMPLE_NOM = "ex.: Isabeau Desrochers";
	public static final String MESSAGE_AFFICHAGE_EXEMPLE_MDP = "ex.: MonMdp";
	
	public static final String MESSAGE_AFFICHAGE_BOUTON_OK = "Ok";
	public static final String MESSAGE_AFFICHAGE_BOUTON_ANNULER = "Annuler";

	public static final String MESSAGE_STATUS_OUVERT = "Statut ouvert: ";
	public static final String MESSAGE_STATUS_EN_TRAITEMENT = "Annuler";
	public static final String MESSAGE_STATUS_SUCCESS = "Annuler";
	public static final String MESSAGE_STATUS_ABANDON = "Annuler";
	
	public static final String ROLE_TECHNICIEN = "technicien";
	public static final String ROLE_CLIENT = "client";
	
}
