package com.cranberrysupport.domaine;

public class Commentaire {

    protected String commentaire;
    protected Utilisateur auteur;
    
    Commentaire(String commentaire, Utilisateur auteur) {
        this.commentaire = commentaire;
        this.auteur = auteur;
    }

    public Utilisateur getAuteur() {
        return auteur;
    }

    public String getComment() {
        return commentaire;
    }

    public String toString() {
        return getAuteur().getNomUtil() + ": " + getComment() + "\n";
    }
}