package com.cranberrysupport.domaine;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import com.cranberrysupport.db.BanqueRequetes;

public class Requete {

    public enum Statut {

        ouvert("ouvert"), enTraitement("en traitement"), finalAbandon("Abandon"), finalSucces("Succès");
        String valeur;

        //Constructeur des statuts
        Statut(String s) {
            this.valeur = s;
        }

        public String getValueString() {
            return valeur;
        }

        //Retourne le statut en fonction d'une string donnée
        public static Statut fromString(String text) {
            if (text != null) {
                if (Statut.ouvert.getValueString().equals(text)) {
                    return Statut.ouvert;
                } else if (Statut.enTraitement.getValueString().equals(text)) {
                    return Statut.enTraitement;
                } else if (Statut.finalAbandon.getValueString().equals(text)) {
                    return Statut.finalAbandon;
                } else if (Statut.finalSucces.getValueString().equals(text)) {
                    return Statut.finalSucces;
                } else if (Statut.finalAbandon.getValueString().equals(text)) {
                    return Statut.finalAbandon;
                }
            }
            return null;
        }
    }

    public enum Categorie {

        posteDeTravail("Poste de travail"), serveur("Serveur"),
        serviceWeb("Service web"), compteUsager("Compte usager"), autre("Autre");
        String value;

        Categorie(String s) {
            this.value = s;
        }

        public String getValueString() {
            return value;
        }

        //Retourne la Catégorie en fonction d'une String donnée
        public static Categorie fromString(String text) {
            if (text != null) {
                if (Categorie.posteDeTravail.getValueString().equals(text)) {
                    return Categorie.posteDeTravail;
                } else if (Categorie.serveur.getValueString().equals(text)) {
                    return Categorie.serveur;
                } else if (Categorie.serviceWeb.getValueString().equals(text)) {
                    return Categorie.serviceWeb;
                } else if (Categorie.compteUsager.getValueString().equals(text)) {
                    return Categorie.compteUsager;
                } else if (Categorie.autre.getValueString().equals(text)) {
                    return Categorie.autre;
                }
            }
            return null;
        }
    }
    
    
    private String sujet = "";
    private String description = "";
    private String pathFichier;
    private File fichier;
    private Integer numero;
    private Utilisateur client;
    private Technicien technicien;
    private Statut statut;
    private Categorie cat;
    private ArrayList<Commentaire> listeCommentaires;
    private Utilisateur tempo;

    //Petit constructeur
    public Requete(String sujet, String description, Client client, Categorie cat) throws FileNotFoundException, IOException {
        this.sujet = sujet;
        this.description = description;
        this.client = client;

        numero = BanqueRequetes.getInstance().incrementeNo();
        statut = Statut.ouvert;
        this.cat = cat;
        listeCommentaires = new ArrayList<Commentaire>();
    }

    //Constructeur complet
    public Requete(String sujet, String descrip, Utilisateur client, Categorie categorie) throws FileNotFoundException, FileNotFoundException, IOException {
        this.client = client;
        this.sujet = sujet;
        this.description = descrip;
        this.tempo = client;
        if (tempo.getRole().equals("client")) {
            client = (Client) tempo;
            tempo = null;
        } else {
            technicien = (Technicien) tempo;
            tempo = null;
        }

        numero = BanqueRequetes.getInstance().incrementeNo();
        statut = Statut.ouvert;
        cat = categorie;
        listeCommentaires = new ArrayList<Commentaire>();
    }

    //Getters et Setters
    public String getSujet() {
        return sujet;
    }
    public void setSujet(String nouveau) {
        sujet = nouveau;
    }
    
    public void setDescription(String nouvelle) {
        description = nouvelle;
    }

    public void uploadFichier() {
        //a faire
    }

    public void setCategorie(Categorie choix) {
        cat = choix;
    }

    public File getFichier() {
        return fichier;
    }

    public void setStatut(Statut newstat) {
        statut = newstat;
    }

    //Finalise
    public void finaliser(Statut fin) {
        this.setStatut(fin);
        this.technicien.ajoutListRequetesFinies(this);
    }

    //Ajout de commentaire dans la liste des commentaires
    public void addCommentaire(String aDire, Utilisateur toi) {
        Commentaire suivant = new Commentaire(aDire, toi);
        listeCommentaires.add(suivant);
    }

    //Il n'y a pas de Technicien au départ à moins qu'il la crée lui-meme
    //A un certain point il faut donc désigner un technicien
    public void setTech(Utilisateur tech) {
        if (tech != null) {
            if (tech.getRole().equals("technicien")) {
                this.technicien = (Technicien) tech;
                technicien.ajouterRequeteAssignee(this);
            }
        }
    }

    public void setFile(File file) {
        fichier = file;
    }
    
    public Integer getNumero() {
        return numero;
    }

    public Statut getStatut() {
        return statut;
    }

    public ArrayList<Commentaire> getComments() {
        return listeCommentaires;
    }

    public Categorie getCategorie() {
        return cat;
    }

    public Technicien getTech() {
        return technicien;
    }

    public Utilisateur getClient() {
        return client;
    }

    public String getDescrip() {
        return description;
    }
}