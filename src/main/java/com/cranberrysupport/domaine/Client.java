package com.cranberrysupport.domaine;


import java.util.ArrayList;

import static com.cranberrysupport.utilitaire.Constante.ROLE_CLIENT;

public class Client extends Utilisateur {

    private ArrayList<Requete> listeRequetesClient;

    //constructeur temporaire
    public Client(String nom, String mdp) {
        super(nom, mdp, ROLE_CLIENT);
        listeRequetesClient = new ArrayList<Requete>();

    }

    //Constructeur de client complet
    public Client(String prenom, String nom, String nomUtilisateur, String mdp) {
        super(prenom, nom, nomUtilisateur, mdp, ROLE_CLIENT);
        listeRequetesClient = new ArrayList<Requete>();
    }

    @Override
    public String getRole() {
        return ROLE_CLIENT;
    }

    @Override
    public void ajoutRequete(Requete nouvelle) {
        listeRequetesClient.add(nouvelle);
    }

    //TODO
    public void trouverRequete() {
    }
    
    @Override
    public ArrayList getListeRequetes() {
        return listeRequetesClient;
    }

    @Override
    public ArrayList<Requete> getListPerso() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public ArrayList<Requete> getListStatut(Requete.Statut statut) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}