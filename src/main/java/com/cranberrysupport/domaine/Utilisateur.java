package com.cranberrysupport.domaine;


import java.util.ArrayList;

import com.cranberrysupport.domaine.Requete.Statut;

public abstract class Utilisateur {

    protected String nom;
    protected String prenom;
    protected String nomUtilisateur;
    protected String motDePasse;
    protected Integer telephone;
    protected String mail;
    protected String bureau;
    public String role;
    public ArrayList info;

    public Utilisateur(String nom, String motDePasse, String role) {
        this.nomUtilisateur = nom;
        this.motDePasse = motDePasse;
        this.role = role;

    }
    //Constructeur complet

    public Utilisateur(String prenom, String nom, String nomUtilisateur, String motDePasse, String role) {
        this.prenom = prenom;
        this.nom = nom;
        this.nomUtilisateur = nomUtilisateur;
        this.motDePasse = motDePasse;
        this.role = "client";
    }

    public String getRole() {
        return role;
    }

    //donne la liste des infos de l'utilisateur
    public ArrayList<String> fetchInfos() {
        info.add(nom);
        info.add(prenom);
        info.add(nomUtilisateur);
        info.add(telephone.toString());
        info.add(mail);
        info.add(bureau);
        info.add(role);
        return info;
    }

    public String getNom() {
        return nomUtilisateur;
    }

    //Verifie les informations à la connexion
    public boolean login(String nomUtilisateur, String motDePasse) {
        if (this.nomUtilisateur.equalsIgnoreCase(nomUtilisateur) && this.motDePasse.equals(motDePasse)) {
            return true;
        } else {
            return false;
        }

    }

    public String getNomUtil() {
        return nomUtilisateur;
    }

    public abstract ArrayList<Requete> getListStatut(Requete.Statut statut);

    public abstract ArrayList<Requete> getListeRequetes();

    public abstract void ajoutRequete(Requete nouvelle);

    public abstract ArrayList<Requete> getListPerso();
}