package com.cranberrysupport.domaine;


import java.util.ArrayList;

import static com.cranberrysupport.utilitaire.Constante.*;

public class Technicien extends Utilisateur {

    //Listes importantes
    private ArrayList<Requete> ListeRequetesTech;
    private ArrayList<Requete> ListeRequetesFinies;
    private ArrayList<Requete> ListeRequetesEnCours;
    private ArrayList<Requete> ListeRequetesPerso;

    //Petit constructeur
    public Technicien(String nom, String mdp) {
        super(nom, mdp, ROLE_TECHNICIEN);
    }

    //Constructeur complet
    public Technicien(String prenom, String nom, String nomUtilisateur, String mdp) {
        super(prenom, nom, nomUtilisateur, mdp, ROLE_TECHNICIEN);
        ListeRequetesTech = new ArrayList<Requete>();
        ListeRequetesEnCours = new ArrayList<Requete>();
        ListeRequetesFinies = new ArrayList<Requete>();
        ListeRequetesPerso = new ArrayList<Requete>();
    }

    @Override
    public String getRole() {
        return ROLE_TECHNICIEN;
    }

    @Override
    public ArrayList getListeRequetes() {
        return ListeRequetesTech;
    }

    //Actions lorsqu'il se fait assigner une requete
    public void ajouterRequeteAssignee(Requete assignee) {
        ListeRequetesEnCours.add(assignee);
        ListeRequetesTech.add(assignee);
    }

    //Action sur les liste lorsqu'une requete est finalisée
    public void ajoutListRequetesFinies(Requete finie) {
        ListeRequetesFinies.add(finie);
        ListeRequetesEnCours.remove(finie);
    }

    //Ajoute une requete (qu'il a lui-même créée)
    @Override
    public void ajoutRequete(Requete nouvelle) {
        ListeRequetesTech.add(nouvelle);
        ListeRequetesPerso.add(nouvelle);
    }

    @Override
    public ArrayList<Requete> getListPerso() {
        return ListeRequetesPerso;
    }

    //Retourne la liste des requetes de ce statut
    @Override
    public ArrayList<Requete> getListStatut(Requete.Statut statut) {
        ArrayList<Requete> r = new ArrayList<Requete>();
        for (int i = 0; i < ListeRequetesTech.size(); i++) {
            if (ListeRequetesTech.get(i).getStatut().equals(statut)) {
                r.add(ListeRequetesTech.get(i));
            }

        }
        return r;
    }

    //Retourne une string contenant pour ce technicien le nombre
    //de requetes en fonction de leur statut
    public String getRequeteParStatut() {
        int ouv = 0;
        int tr = 0;
        int su = 0;
        int ab = 0;
        String parStatut = "";
        for (int i = 0; i < ListeRequetesTech.size(); i++) {
            if (ListeRequetesTech.get(i).getStatut().equals(Requete.Statut.ouvert)) {
                ouv++;
            }
            if (ListeRequetesTech.get(i).getStatut().equals(Requete.Statut.enTraitement)) {
                tr++;
            }
            if (ListeRequetesTech.get(i).getStatut().equals(Requete.Statut.finalSucces)) {
                su++;
            }
            if (ListeRequetesTech.get(i).getStatut().equals(Requete.Statut.finalAbandon)) {
                ab++;
            }
        }

        parStatut = MESSAGE_STATUS_OUVERT 		 + ouv + "\n"
                  + MESSAGE_STATUS_EN_TRAITEMENT + tr + "\n"
                  + MESSAGE_STATUS_SUCCESS 		 + su + "\n"
                  + MESSAGE_STATUS_ABANDON 		 + ab + "\n";
        return parStatut;

    }
}