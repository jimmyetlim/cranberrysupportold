package com.cranberrysupport.db;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import com.cranberrysupport.domaine.Requete;
import com.cranberrysupport.domaine.Technicien;
import com.cranberrysupport.domaine.Utilisateur;

public class BanqueRequetes {

    private static BanqueRequetes instance = null;
    private Integer numero = -1;
    private ArrayList<Requete> listeRequetes;

    //Constructeur privé
    private BanqueRequetes() throws FileNotFoundException, IOException {
        listeRequetes = new ArrayList<Requete>(100);
    }

    //Il s'agit d'un singleton
    public static BanqueRequetes getInstance() throws FileNotFoundException, IOException {
        if (instance != null) {
            return instance;
        } else {
            instance = new BanqueRequetes();
            instance.chargerRequetes();
            return instance;

        }
    }

    //nouvelle requête
    public void newRequete(String sujet, String desrcrip, Utilisateur client, Requete.Categorie cat)
            throws FileNotFoundException, IOException {
        Requete nouvelle = new Requete(sujet, desrcrip, client, cat);
        listeRequetes.add(nouvelle);
        client.ajoutRequete(nouvelle);


    }

    //Retourne la liste des requête en fonction de ce Statut
    public ArrayList getListRequetes(Requete.Statut statut) {
        ArrayList<Requete> ceStatut = new ArrayList<Requete>();
        for (int i = 0; i < listeRequetes.size(); i++) {
            if (listeRequetes.get(i).getStatut().equals(statut)) {
                ceStatut.add(listeRequetes.get(i));
            }

        }
        return ceStatut;
    }

    //Assigne une requête à un technicien
    public void assignerRequete(Technicien technicien, Requete selectionee) {
        selectionee.setStatut(Requete.Statut.enTraitement);
        selectionee.setTech(technicien);
    }

    //Retourne la dernière requête de la liste
    public Requete returnLast() {
        return listeRequetes.get(listeRequetes.size() - 1);
    }

    //Méthode de génération du numéro de la requête lorsqu'on la crée
    public Integer incrementeNo() {
        return ++numero;
    }

    //Méthode du remplissage de la liste de requête à l'ouverture
    private void chargerRequetes() throws FileNotFoundException, IOException {
        File f = new File("dat/BanqueRequetes.txt");
        if (f.exists()) {
            Scanner banqueRIn = new Scanner(new FileReader(f));
            while (banqueRIn.hasNextLine()) {
                String s = banqueRIn.nextLine();
                String[] ss = s.split(";~", -1);
                this.newRequeteString(ss[0], ss[1], ss[2], ss[3], ss[4], ss[5], ss[6], ss[7]);
            }
        }
    }

    //Methode de création d'une requête à partir du fichier texte ou elle
    //est sauvegardée
    private void newRequeteString(String sujet, String descrip, String client,
            String categorie, String statut, String path, String tech, String comm)
            throws FileNotFoundException, IOException {
        Requete nouvelle = new Requete(sujet, descrip,
                BanqueUtilisateurs.getUserInstance().chercherNomRole(client),
                Requete.Categorie.fromString(categorie));
        nouvelle.setStatut(Requete.Statut.fromString(statut));
        File tempo = new File(path);
        nouvelle.setFile(tempo);
        nouvelle.setTech(BanqueUtilisateurs.getUserInstance().chercherNomRole(tech));

        if (!comm.equals("")) {
            String[] comms = comm.split("/%");
            for (int i = 0; i < comms.length; i++) {
                String[] ceCommentaire = comms[i].split("&#");
                nouvelle.addCommentaire(ceCommentaire[0],
                        BanqueUtilisateurs.getUserInstance().chercherNomRole(ceCommentaire[1]));
            }
        }
        listeRequetes.add(nouvelle);
        nouvelle.getClient().ajoutRequete(nouvelle);


    }
    //Méthode qui enregistre toutes les requêtes à la fermeture de l'app

    public void saveRequetes() throws IOException {
        FileWriter sauvegarde = new FileWriter("dat/BanqueRequetes.txt");
        BufferedWriter saving = new BufferedWriter(sauvegarde);
        for (int i = 0; i < listeRequetes.size(); i++) {
            String comments = "";
            if (listeRequetes.get(i).getComments() != null) {
                for (int j = 0; j < listeRequetes.get(i).getComments().size(); j++) {
                    comments += listeRequetes.get(i).getComments().get(j).getComment();
                    comments += "&#" + listeRequetes.get(i).getComments().get(j).getAuteur().getNomUtil();
                    if (listeRequetes.get(i).getComments().size() - 1 == j) {
                        comments += "";
                    } else {
                        comments += "/%";
                    }
                }
            }
            final String sep = ";~";
            String path = "";
            String tech = "";
            String cat = listeRequetes.get(i).getCategorie().getValueString();
            if (listeRequetes.get(i).getTech() != null) {
                tech = listeRequetes.get(i).getTech().getNomUtil();
            }
            if (listeRequetes.get(i).getFichier() != null) {
                path = listeRequetes.get(i).getFichier().getPath();
            }
            String s = listeRequetes.get(i).getSujet() + sep
                    + listeRequetes.get(i).getDescrip() + sep
                    + listeRequetes.get(i).getClient().getNomUtil() + sep
                    + cat + sep
                    + listeRequetes.get(i).getStatut().getValueString() + sep
                    + path + sep + tech + sep 
                    + comments + "\n";

            saving.write(s);
        }
        //Ferme le stream
        saving.close();

    }
}
